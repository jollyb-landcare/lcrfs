"""
Utility functions for dealing with 'lcrfs' filenames.
"""

import os

class LCRFSException(Exception):
    pass

def change(fname, part, newpart, tag=None):
    """
    Change the part of fname - replace with newpart
    """
    dirfname = os.path.dirname(fname)
    fname = os.path.basename(fname) # get rid of path
    fname, ext = os.path.splitext(fname) # get rid of extension

    parts = fname.split('_')
    if part == 'satellite':
        parts[0] = newpart
    elif part == 'sensor':
        parts[1] = newpart
    elif part == 'location':
        parts[2] = newpart
    elif part == 'date':
        parts[3] = newpart
    elif part == 'stage':
        parts[4] = newpart
    elif part == 'proj':
        parts[5] = newpart
    elif part == 'option':
        if tag is None:
            raise LCRFSException('tag must be given for option field')
        tagdict = {}
        for part in parts[6:]:
            tagdict[part[0]] = part[1:]

        del parts[6:]

        tagdict[tag] = newpart
        for tag in sorted(tagdict.keys()):
            value = tagdict[tag]
            if value is not None:
                parts.append(tag + value)

    elif part == 'ext':
        ext = ext[0] + newpart

    # rebuild fname
    fname = '_'.join(parts)
    newname = os.path.join(dirfname, fname + ext)
    return newname

def lcrfs(cmd, fname, tag=None):
    """
    process cmd and return result
    """
    fname = os.path.basename(fname) # get rid of path
    fname, ext = os.path.splitext(fname) # get rid of extension

    parts = fname.split('_')
    if len(parts) < 6:
        raise LCRFSException("%s does not appear to be a valid filename" % fname)
    retVal = None

    if cmd == 'date':
        retVal = parts[3]

    elif cmd == 'ext':
        retVal = ext[1:] # ignore the .

    elif cmd == 'location':
        retVal = parts[2]

    elif cmd == 'path':
        BASE = os.path.expandvars('$LCR_DATA/base/raster')
        satellite = parts[0]
        sensor = parts[1]
        location = parts[2]
        date = parts[3]
        year = date[:2]
        if int(year) < 50:
            year = '20' + year
        else:
            year = '19' + year

        if satellite.startswith('spot'):
            if sensor.startswith('hrg'):
                sensor = 'hrg'
            path = os.path.join(BASE, satellite, sensor, year, location)

        elif satellite.startswith('landsat'):
            path = os.path.join(BASE, satellite, year, location)

        elif sensor == 'modis':
            path = os.path.join(BASE, sensor, year, location)
            
        elif satellite == 'alos':
            path = os.path.join(BASE, satellite, date, location)

        elif satellite.startswith('sentinel'):
            path = os.path.join(BASE, satellite, year, location)

        else:
            raise LCRFSException("%s does not appear to be a valid filename" % fname)

        retVal = path

    elif cmd == 'proj':
        retVal = parts[5]

    elif cmd == 'satellite':
        retVal = parts[0]

    elif cmd == 'sensor':
        retVal = parts[1]

    elif cmd == 'stage':
        retVal = parts[4]

    elif cmd == 'option':
        if tag is None:
            raise LCRFSException("must pass tag for option field")
        tagdict = {}
        for part in parts[6:]:
            tagdict[part[0]] = part[1:]

        retVal = tagdict[tag]
    
    else:
        raise LCRFSException("Invalid command %s" % cmd)

    return retVal

def getMTLFileForUSGS(fname):
    """
    Returns the full path to the associated .mtl file for a given
    landsat image.
    
    """
    # make sure we get right directory
    fname = os.path.basename(fname)
    mtlFile = os.path.join(lcrfs('path', fname), fname)
    
    mtlFile = change(mtlFile, 'stage', 'rect')
    mtlFile = change(mtlFile, 'ext', 'mtl')
    sensor = lcrfs('sensor', mtlFile)
    sensor = sensor[:-2] + 're'
    mtlFile = change(mtlFile, 'sensor', sensor)
    # only way to find the zone - just check
    for zone in (58, 59, 60, 61):
        mtlFile = change(mtlFile, 'proj', 'utm%d' % zone)
        if os.path.exists(mtlFile):
            break
            
    if not os.path.exists(mtlFile):
        msg = 'could not find mtl file %s' % mtlFile
        raise LCRFSException(msg)
                    
    return mtlFile
