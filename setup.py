from distutils.core import setup

setup(
    name = "lcrfs",
    packages = ["lcrfs"],
    scripts=[
        "bin/lcrfs",
        "bin/lcrfs_change"
    ],
    version = "1.0.0",
    description = "Landcare file system helper",
    author = "Sam Gillingham",
    author_email = "jollyb@landcareresearch.co.nz",
    url = "git@bitbucket.org:jollyb-landcare/lcrfs.git",
    download_url = "",
    keywords = ["satellite", "landcare"],
    classifiers = [
        "Programming Language :: Python :: 3",
        "Development Status :: 5 - Production",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
    long_description = """\
This version requires Python 3 or later.
"""
)

